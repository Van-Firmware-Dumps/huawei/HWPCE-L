#!/bin/sh
#######################################################################
##- Copyright (c) Huawei Technologies Co., Ltd. 2019. All rights reserved.
##- @Description: make dir for isulad, to reduce dac right of ohos_isula
##- @Author: guojiawei
##- @Create: 2022-06-27
#######################################################################*/

# ensure /data/isula is exist
mkdir -p /data/isula/rpm
# HSL-USB: To be shared mount dir
mkdir -p /data/isula/usb

# HSL-USB: usb module needs +r for others
chmod 715 /data/isula
chmod 1777 /data/isula/rpm
# HSL-USB: to be read/write by linux app
chmod 1777 /data/isula/usb

# rootfs update, need root-fs exist
mkdir /data/isula/root-fs
# ensure log dir is exist
mkdir -p /data/log/hsl_share

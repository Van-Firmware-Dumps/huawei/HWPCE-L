#!/system/bin/sh
IFS=""

REG="[a-zA-Z0-9]{64}"

containerId=`echo "${LXC_CONFIG_FILE}" | grep -o -E "$REG"`
chmod 1777 /data/isula/var/lib/isulad/engines/lcr/"$containerId"/mounts/shm
chmod 1706 /data/isula/hsl/.local
set -- "$@" "lxc.mount.entry = tmpfs dev tmpfs nosuid,strictatime,mode=755,size=65536k,create=dir 0 0"
set -- "$@" "lxc.mount.entry = devpts dev/pts devpts nosuid,noexec,newinstance,ptmxmode=0666,mode=0620,gid=5,create=dir 0 0"
set -- "$@" "lxc.mount.entry = mqueue dev/mqueue mqueue nosuid,noexec,nodev,create=dir 0 0"
set -- "$@" "lxc.mount.entry = /data/isula/usb tablet/usb_share bind slave,rw,rbind,create=dir 0 0"
set -- "$@" "lxc.mount.entry = /data/log/hsl_share data/log/hsl_share bind rw,rprivate,rbind,create=dir 0 0"
set -- "$@" "lxc.mount.entry = /dev/ashmem hostdev/ashmem bind rw,rprivate,rbind,create=file 0 0"
set -- "$@" "lxc.mount.entry = /dev/hsl/rdp dev/hsl/rdp bind rw,rprivate,rbind,create=dir 0 0"
set -- "$@" "lxc.mount.entry = /sdcard/PCEngine home/hsl bind rw,rprivate,rbind,create=dir 0 0"
set -- "$@" "lxc.mount.entry = /data/isula/hsl/.cache home/hsl/.cache bind rw,rprivate,rbind,create=dir 0 0"
set -- "$@" "lxc.mount.entry = /data/isula/hsl/.config home/hsl/.config bind rw,rprivate,rbind,create=dir 0 0"
set -- "$@" "lxc.mount.entry = /data/isula/hsl/.dbus home/hsl/.dbus bind rw,rprivate,rbind,create=dir 0 0"
set -- "$@" "lxc.mount.entry = /data/isula/hsl/.fontconfig home/hsl/.fontconfig bind rw,rprivate,rbind,create=dir 0 0"
set -- "$@" "lxc.mount.entry = /data/isula/hsl/.local home/hsl/.local bind rw,rprivate,rbind,create=dir 0 0"
set -- "$@" "lxc.mount.entry = /sdcard tablet bind rw,rprivate,rbind,create=dir 0 0"
set -- "$@" "lxc.mount.entry = /system/fonts usr/share/fonts/fonts bind rw,rprivate,rbind,create=dir 0 0"
set -- "$@" "lxc.mount.entry = /data/isula/rpm home/hsl/rpm bind rw,rprivate,rbind,create=dir 0 0"
set -- "$@" "lxc.mount.entry = /data/isula/var/lib/isulad/engines/lcr/"$containerId"/mounts/shm dev/shm bind rbind,rprivate,size=67108864,create=dir 0 0"
set -- "$@" "lxc.mount.entry = /mnt/isula/var/lib/isulad/engines/lcr/"$containerId"/hostname etc/hostname bind rbind,rprivate,create=file 0 0"
set -- "$@" "lxc.mount.entry = /mnt/isula/var/lib/isulad/engines/lcr/"$containerId"/hosts etc/hosts bind rbind,rprivate,create=file 0 0"
set -- "$@" "lxc.mount.entry = /mnt/isula/var/lib/isulad/engines/lcr/"$containerId"/resolv.conf etc/resolv.conf bind rbind,rprivate,create=file 0 0"

ret=0

cat ${LXC_CONFIG_FILE} | grep lxc.mount.entry | while read -r rows
do 
  newrows=$rows
  for i in do "$@"; do
    if test "$newrows" = "$i"; then
      continue 2
    fi
  done 
  exit 99
done
ret=$?

exit $ret
